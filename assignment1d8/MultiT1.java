package assignment1d8;

import java.io.FileWriter;
import java.util.ArrayList;

public class MultiT1 extends Thread{

    private ArrayList<Model> nilai = new ArrayList();

    public MultiT1 (ArrayList <Model> nilai)  {
        this.nilai = nilai;
    }


    public void run() {
        String fw = new String("/Users/ada-nb187/Documents/d08/src/assignment1d8/fileWriter.txt");

        try {
            FileWriter fileBaru = new FileWriter(fw);
            fileBaru.write("Nama, averageNilai");
            for (Model el: nilai) {

                double fis = el.getNilaiFis();
                double kim = el.getNilaiKim();
                double bio = el.getNilaiBio();

                double sumNilai = fis + kim + bio;
                double avgNilai = (sumNilai)/3;

                fileBaru.write("\n"+ el.getNama() + "," + avgNilai + "\n");
            }
            fileBaru.close();
        } catch (Exception e) {
            System.out.println(e);
        }


    }
}
