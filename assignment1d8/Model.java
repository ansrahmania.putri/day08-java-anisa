package assignment1d8;

public class Model {
    String nama;
    double nilaiFis;
    double nilaiKim;
    double nilaiBio;

    public Model(String nama, double nilaiFis, double nilaiKim, double nilaiBio) {
        this.nama = nama;
        this.nilaiFis = nilaiFis;
        this.nilaiKim = nilaiKim;
        this.nilaiBio = nilaiBio;
    }

    public String getNama() {
        return nama;
    }
    public double getNilaiFis() {
        return nilaiFis;
    }

    public double getNilaiKim() {
        return nilaiKim;
    }

    public double getNilaiBio() {
        return nilaiBio;
    }

}
