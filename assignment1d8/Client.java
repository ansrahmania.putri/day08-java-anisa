package assignment1d8;

import java.io.*;
import java.net.*;
import java.util.*;

public class Client {
    static Scanner input = new Scanner(System.in);
    static ArrayList<Model> mhs = new ArrayList<>();
    static DataOutputStream dout;
    static Socket s;

    static int choice;
    static String inputIP, inputPort, str;

    public static void showMenu() {
        System.out.println("\nMenu");
        System.out.println("1. Connect Socket");
        System.out.println("2. Create File Process");
        System.out.println("3. Connect FT");
        System.out.println("4. Disconnected");
        System.out.println("0. Exit");

        System.out.print("Pilih menu: ");
        choice = input.nextInt();
    }
    public static void readConfig() {
        try (InputStream inp = new FileInputStream("/Users/ada-nb187/Documents/d08/src/assignment1d8/config.properties")) {

            Properties prop = new Properties();

            prop.load(inp);

            inputIP = prop.getProperty("IP");
            inputPort = prop.getProperty("Port");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void clientSocket() throws IOException {
        System.out.println("\n[Connect Client Socket]");

        s = new Socket(inputIP, Integer.parseInt(inputPort));

        //Send
        dout = new DataOutputStream(s.getOutputStream());
        dout.writeUTF("Get Data");
        dout.flush();

        //Receive
        DataInputStream dis = new DataInputStream(s.getInputStream());
        str = dis.readUTF();
        System.out.println("Server: \n"+str);
    }

    public static void parsingData() {
        System.out.println("\n[Parsing Data]");

        String[] strAccuParsed = str.split("\n");

        for(int j = 0; j < strAccuParsed.length; j++) {
            System.out.println(strAccuParsed[j]);
            if(j > 0) {
                String[] strAccuParsed2 = strAccuParsed[j].trim().split(",");

                String nama = strAccuParsed2[0];
                double fisika = Integer.parseInt(strAccuParsed2[1]);
                double kimia = Integer.parseInt(strAccuParsed2[2]);
                double biologi = Integer.parseInt(strAccuParsed2[3]);

                Model s = new Model(nama, fisika, kimia, biologi);
                mhs.add(s);
            }
        }
    }

    public static void addNewFile() {
        System.out.println("\n[Tambah Data ke File Baru]");

        String newFile = "/Users/ada-nb187/Documents/d08/src/assignment1d8/fileWriter.txt";

        try{
            FileWriter fw = new FileWriter(newFile);

            for (Model m : mhs) {
                fw.write("Nama: "+m.getNama()+"\nNilai Fisika: "+m.getNilaiFis()+"\nNilai Kimia: "+m.getNilaiKim()+"\nNilai Biologi: "+m.getNilaiBio()+"\n\n");
            }

            fw.close();
        }catch(Exception e){
            System.out.println(e);
        }

        System.out.println("Success...");
    }

    public static void multiThreading() {
        System.out.println("\n[Multithread]");

        MultiT1 t1 = new MultiT1(mhs);
        t1.start();

        MultiT2 t2 = new MultiT2();
        t2.start();
    }

    public static void main(String[] args) {
        try{
            readConfig();
            do {
                showMenu();
                switch (choice) {
                    case 1 :
                        clientSocket();
                        parsingData();
                        break;
                    case 2 :
                        addNewFile();
                        break;
                    case 3 :
                        multiThreading();
                        break;
                    case 4 :
                        dout.close();
                        s.close();
                        System.out.println("Koneksi Terputus");
                        break;
                    default:
                        if (choice == 0) {
                            break;
                        } else {
                            System.out.println("Menu yang anda pilih tidak ditemukan");
                        }
                }
            } while (choice != 0);
            dout.close();
            s.close();
        }catch(Exception e){
            System.out.println(e);
        }

        input.close();
    }
}
