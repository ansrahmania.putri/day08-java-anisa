package assignment1d8;

import org.apache.commons.net.ftp.FTP;

import java.io.*;

import org.apache.commons.net.ftp.FTPClient;

public class MultiT2 extends Thread {
    public void run() {
        String server = "ftp.dharmakertamandiri.co.id";
        int port = 21;
        String user = "insanastra@dharmakertamandiri.co.id";
        String pass = "P@ssw0rd12345";

        FTPClient ftpClient = new FTPClient();

        try {
            ftpClient.connect(server, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();

            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            File firstLocalFile = new File("/Users/ada-nb187/Documents/d08/src/assignment1d8/fileWriter.txt");

            String firstRemoteFile = "fileWriterAnisa.txt";
            InputStream inputStream = new FileInputStream(firstLocalFile);

            System.out.println("Start uploading first file");
            boolean done = ftpClient.storeFile(firstRemoteFile, inputStream);
            inputStream.close();
            if (done) {
                System.out.println("The first file is uploaded successfully.");
            }

        } catch (
                IOException ex) {
            System.out.println("Error: " + ex.getMessage());
            ex.printStackTrace();
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}