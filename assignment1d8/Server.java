package assignment1d8;

import java.io.*;
import java.net.*;
import java.util.*;

public class Server {
    static Scanner input = new Scanner(System.in);
    static ArrayList<Model> mhs = new ArrayList<>();
    static String inputPort;
    static ServerSocket ss;
    static String[] strAccuParsed;
    static StringBuilder strAccu;

    public static void readConfig() {
        try (InputStream inp = new FileInputStream("/Users/ada-nb187/Documents/d08/src/assignment1d8/config.properties")) {

            Properties prop = new Properties();

            prop.load(inp);

            inputPort = prop.getProperty("Port");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void readData() throws IOException {
        System.out.println("\n[Parsing Data]");

        String file = "/Users/ada-nb187/Documents/d08/src/assignment1d8/file.txt";

        FileReader fr = new FileReader(file);

        int i;
        strAccu = new StringBuilder();
        while((i=fr.read()) != -1) {
            strAccu.append((char) i);
        }
        System.out.println(strAccu);

        fr.close();
    }

    public static void serverSocket() throws IOException {
        System.out.println("\n[Connect Server Socket]");

        ss = new ServerSocket(Integer.parseInt(inputPort));
        Socket s = ss.accept();

        //Receive
        DataInputStream dis = new DataInputStream(s.getInputStream());
        String str = dis.readUTF();
        System.out.println("Client: "+str);

        //Send
        DataOutputStream dout = new DataOutputStream(s.getOutputStream());
        dout.writeUTF(String.valueOf(strAccu));
        dout.flush();
    }

    public static void main(String[] args) {
        try{
            readConfig();
            readData();
            serverSocket();
            ss.close();
        }catch(Exception e){
            System.out.println(e);
        }

        input.close();
    }
}
