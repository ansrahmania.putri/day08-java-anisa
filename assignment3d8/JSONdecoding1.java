package assignment3d8;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class JSONdecoding1 {

    public static void main(String[] args) throws IOException, ParseException {

    String json = "{\n" +
            "\"name\":\"John\",\n" +
            "\"age\":30,\n" +
            "\"cars\": [\n" +
            "{ \"name\":\"Ford\", \"models\":[ \"Fiesta\", \"Focus\", \"Mustang\" ] },\n" +
            "{ \"name\":\"BMW\", \"models\":[ \"320\", \"X3\", \"X5\" ] },\n" +
            "{ \"name\":\"Fiat\", \"models\":[ \"500\", \"Panda\" ] }\n" +
            "]\n" +
            "}";

    JSONParser parser = new JSONParser();
    Reader reader = new StringReader(json);

    Object jsonObj = parser.parse(reader);

    JSONObject jsonObject = (JSONObject) jsonObj;

    String name = (String) jsonObject.get("name");
    System.out.println("Name = " + name);

    long age = (long) jsonObject.get("age");
    System.out.println("Age = " + age);

    System.out.println("----------------------");

    ArrayList<JSONObject> alCars = (ArrayList<JSONObject>) jsonObject.get("cars");
    for (JSONObject car : alCars) {
        String namaMobil = (String) car.get("name");
        System.out.println("Cars = " + namaMobil);
        ArrayList<String> modelMobil = (ArrayList<String>) car.get("models");
        for (String model : modelMobil){
            System.out.println("Models = " + model);
        }
        System.out.println("----------------------");
    }

}
}
